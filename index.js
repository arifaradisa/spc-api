var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var PATH = require('path');
var directory = PATH.join(__dirname, '/uploads');

app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());

app.use('/uploads', express.static(directory));

require('./app/router/router.js')(app);

const db = require('./app/config/db.config.js');

var server = app.listen(8181, function () {
 
  var host = server.address().address
  var port = server.address().port
 
  console.log("App listening at http://%s:%s", host, port)
})
