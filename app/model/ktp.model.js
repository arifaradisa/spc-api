module.exports = (sequelize, Sequelize) => {
	const Ktp = sequelize.define('ktp', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true
          },
        nik: {
            type: Sequelize.STRING,
            unique: true
        },
        nama: {
            type: Sequelize.STRING
        },
        tmp_lahir: {
            type: Sequelize.STRING
        },
        tgl_lahir: {
            type: Sequelize.DATE
        },
        jenis_kelamin: {
            type: Sequelize.STRING
        },
        gol_darah: {
            type: Sequelize.STRING
        },
        alamat: {
            type: Sequelize.STRING
        },
        rt: {
            type: Sequelize.STRING
        },
        rw: {
            type: Sequelize.STRING
        },
        kelurahan: {
            type: Sequelize.STRING
        },
        kecamatan: {
            type: Sequelize.STRING
        },
        kabupaten: {
            type: Sequelize.STRING
        },
        provinsi: {
            type: Sequelize.STRING
        },
        agama: {
            type: Sequelize.STRING
        },
        perkawinan: {
            type: Sequelize.STRING
        },
        pekerjaan: {
            type: Sequelize.STRING
        },
        warga_negara: {
            type: Sequelize.STRING
        },
        berlaku: {
            type: Sequelize.STRING
        },
        tmp_keluar: {
            type: Sequelize.STRING
        },
        tgl_keluar: {
            type: Sequelize.STRING
        },
        img_path: {
            type: Sequelize.STRING
        }
	},{
        timestamps: false,
        freezeTableName: true,
    });
	
	return Ktp;
}