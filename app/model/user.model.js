module.exports = (sequelize, Sequelize) => {
	const User = sequelize.define('user', {
        user_id: {
            type: Sequelize.INTEGER,
            primaryKey: true
          },
        username: {
            type: Sequelize.STRING
        },
        password: {
            type: Sequelize.STRING
        },
        is_login: {
            type: Sequelize.BOOLEAN
        }
	},{
        timestamps: false,
        freezeTableName: true,
    });
	
	return User;
}