const db = require('../config/db.config.js');
const response = require('../config/response.js');
const multer  = require('multer');
const fs = require('fs');

const Ktp = db.ktp;

var storage = multer.diskStorage(
    {
        destination: './uploads/',
        filename: function ( req, file, cb ) {
            cb( null, file.originalname);
        }
    }
);

var upload = multer({ storage: storage });

exports.add = (req, res) => {
    console.log("Add-Ktp");

    upload.single('image')(req, res, function (err) {
        if (err instanceof multer.MulterError) {
          response.fail(500,{ error: err }, res);
          console.log("Upload failed");
        } else if (err) {
          response.fail(500,{ error: err }, res);
          console.log("Upload failed");
        }else{
            console.log("Upload success");
            Ktp.create({
                nik: req.body.nik,
                nama: req.body.nama,
                tmp_lahir: req.body.tmp_lahir,
                tgl_lahir: req.body.tgl_lahir,
                jenis_kelamin: req.body.jenis_kelamin,
                gol_darah: req.body.gol_darah,
                alamat: req.body.alamat,
                rt: req.body.rt,
                rw: req.body.rw,
                kelurahan: req.body.kelurahan,
                kecamatan: req.body.kecamatan,
                kabupaten: req.body.kabupaten,
                provinsi: req.body.provinsi,
                agama: req.body.agama,
                perkawinan: req.body.perkawinan,
                pekerjaan: req.body.pekerjaan,
                warga_negara: req.body.warga_negara,
                berlaku: req.body.berlaku,
                tmp_keluar: req.body.tmp_keluar,
                tgl_keluar: req.body.tgl_keluar,
                img_path: req.body.img_path
            }).then(ktp => {
                response.ok({message: "Data inserted"}, res);
            }).catch(err => {
                response.fail(500,{ error: err.parent.sqlMessage }, res);
            });     
        }
    })
}

exports.list = (req, res) => {
    console.log("List-KTP");
    const pageSize = 10;
    const offset = (req.query.page-1) * pageSize;
    const limit = pageSize;

    Ktp.findAll({
        order: [
            ['id', 'DESC'],
        ],
        offset: offset,
        limit: limit
    }).then(ktp => {
        response.ok({ktp: ktp}, res);
    }).catch(err => {
        response.fail(500,{ error: err.parent.sqlMessage }, res);
    });
}

exports.delete = async (req, res) => {
    console.log("Delete-KTP");

    var ktp = await Ktp.findOne({
        where: {
            id: req.body.id
        }
    }).then(ktp => {
        return ktp;
    });

    fs.unlink("."+ktp.img_path, (err) => {
        if (err) {
            return response.fail(500,{ error: err }, res);   
        }
        console.log('Image was deleted');

        ktp.destroy().then(ktp => {
            response.ok({message: "Data has successfully deleted"}, res);
        }).catch(err => {
            response.fail(500,{ error: err.parent.sqlMessage }, res);
        });
    });
}

exports.update = (req, res) => {
    console.log("Update-KTP");

    upload.single('image')(req, res, function (err) {
        if (err instanceof multer.MulterError) {
          response.fail(500,{ error: err }, res);
          console.log("Upload failed");
        } else if (err) {
          response.fail(500,{ error: err }, res);
          console.log("Upload failed");
        }else{
            console.log("Upload success");     
        }

        Ktp.findOne({
            where: {
                id: req.body.id
            }
        }).then(ktp => {
            ktp.update({
                nama: req.body.nama,
                tmp_lahir: req.body.tmp_lahir,
                tgl_lahir: req.body.tgl_lahir,
                jenis_kelamin: req.body.jenis_kelamin,
                gol_darah: req.body.gol_darah,
                alamat: req.body.alamat,
                rt: req.body.rt,
                rw: req.body.rw,
                kelurahan: req.body.kelurahan,
                kecamatan: req.body.kecamatan,
                kabupaten: req.body.kabupaten,
                provinsi: req.body.provinsi,
                agama: req.body.agama,
                perkawinan: req.body.perkawinan,
                pekerjaan: req.body.pekerjaan,
                warga_negara: req.body.warga_negara,
                berlaku: req.body.berlaku,
                tmp_keluar: req.body.tmp_keluar,
                tgl_keluar: req.body.tgl_keluar
            }).then(ktp => {
                response.ok({message: "Data has successfully updated"}, res);
            }).catch(err => {
                response.fail(500,{ error: err.parent.sqlMessage }, res);
            });
        }).catch(err => {
            response.fail(500,{ error: err }, res);
        });
    })
}

exports.detail = (req, res) => {
    console.log("Update-KTP");

    Ktp.findOne({
        where: {
            id: req.query.id
        }
    }).then(ktp => {
        response.ok({detail: ktp}, res);
    }).catch(err => {
        response.fail(500,{ error: err.parent.sqlMessage }, res);
    });
}