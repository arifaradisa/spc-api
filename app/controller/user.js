const db = require('../config/db.config.js');
const config = require('../config/config.js');
const response = require('../config/response.js');

const sequelize = db.sequelize;
const User = db.user;

var crypto = require('crypto');
var jwt = require('jsonwebtoken');

exports.signin = (req, res) => {
	console.log("Sign-In");

	User.findOne({
		where: {
			Username: req.body.username
		}
	}).then(user => {
		if (!user) {
			return response.notfound(res);
		}
		var firstEnc = crypto.createHash('sha1').update(req.body.password).digest('hex');
		var passwordIsValid = firstEnc==user.password?true:false;
		if (!passwordIsValid) {
			return response.fail(401, {reason: "Invalid username or password" }, res);
		}
		
		var token = jwt.sign({ user_id: user.user_id }, config.secret, {
		  expiresIn: 86400*3 // expires in 3 days
		});
		
    response.ok({accessToken: token, username: req.body.username}, res);
		
	}).catch(err => {
        response.fail(500,{ error: err }, res)
	});
}

exports.signout = (req, res) => {
	console.log("Sign-Out");

	User.findOne({
		where: {
			user_id: req.userId
		}
	}).then(user => {
		if (!user) {
			response.notfound(res);
		}else{
            user.update({is_login: false});
            response.ok({message: "You have not access anymore to any data"}, res);
        }		
	}).catch(err => {
        response.fail(500,{ error: err }, res)
	});
}