exports.ok = function(values, res) {
    var data = {
        'status': true,
        'status_code': 200,
        'message': "Data successfully proceed",
        'result': values
    };
    res.json(data);
    res.end();
};

exports.fail = function(code, values, res) {
    var data = {
        'status': false,
        'status_code': code,
        'message': "Failed to process data",
        'result': values
    };
    res.json(data);
    res.end();
};

exports.notfound = function(res) {
    var data = {
        'status': false,
        'status_code': 404,
        'message': "Data not found",
        'result': {}
    };
    res.json(data);
    res.end();
};