const authJwt = require('./verifyJwtToken');

module.exports = function(app) {

    const user = require('../controller/user.js');
    const ktp = require('../controller/ktp.js');

    app.get('/', (req, res) => {
        res.send("Hello developers :)")
    });
    app.post('/v1/user/signin', user.signin);
    app.get('/v1/user/signout', [authJwt.verifyToken], user.signout);
    
    app.post('/v1/ktp/add', [authJwt.verifyToken], ktp.add);
    app.get('/v1/ktp/list', [authJwt.verifyToken], ktp.list);
    app.post('/v1/ktp/remove', [authJwt.verifyToken], ktp.delete);
    app.post('/v1/ktp/update', [authJwt.verifyToken], ktp.update);
    app.get('/v1/ktp/detail', [authJwt.verifyToken], ktp.detail);
}
