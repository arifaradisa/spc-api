const jwt = require('jsonwebtoken');
const config = require('../config/config.js');
const response = require('../config/response.js');

verifyToken = (req, res, next) => {
	let token = req.headers['x-access-token'];
  
	if (!token){
        return response.fail(403, {message: 'No token provided'}, res);
	}

	jwt.verify(token, config.secret, (err, decoded) => {
		if (err){
            return response.fail(123, {message: 'Fail to Authentication. Error -> ' + err}, res);
		}
        req.userId = decoded.user_id;
		next();
	});
}

const authJwt = {};
authJwt.verifyToken = verifyToken;

module.exports = authJwt;