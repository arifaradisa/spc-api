FROM node:8.15.0-alpine

WORKDIR /app

COPY package.json .

RUN npm install mysql2 -g
RUN npm install

COPY . .

CMD [ "npm", "run", "start" ]